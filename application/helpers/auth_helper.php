<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function admin_auth()
{
  if(!isset($_SESSION['admin_user_id']))
  {
    $msg = 'You are not allowd here!!!';
    $ci =& get_instance();
    $ci->session->set_flashdata('item', $msg);
    redirect(base_url());
  }
}
function user_auth()
{
  if(!isset($_SESSION['user_id']))
  {
    redirect('user/login');
  }
}
function _password_verify($password,$hash)
{
  return (password_verify($password,$hash))? true : false;
}
