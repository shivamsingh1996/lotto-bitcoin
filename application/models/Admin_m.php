<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_m extends CI_Model
{

    function __construct() {
       //parent::__construct();
    }

    function _custom_query($query) {
        $query = $this->db->query($query);
        return $query;
    }
   	
    function _update($table,$where_clause,$where_param,$data){
    	$this->db->update($table, $data, "$where_clause = $where_param");
    }

    function insert($table,$data){
    	$this->db->insert($table,$data);
    }
}
