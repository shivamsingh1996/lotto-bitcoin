<!-- footer content -->
<footer>
  <div class="pull-right">
    2018 <a href="https://colorlib.com">Bitcoin</a>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- FastClick -->
<script src="<?=base_url('public/admin')?>/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?=base_url('public/admin')?>/vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?=base_url('public/admin')?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url('public/admin')?>/vendors/iCheck/icheck.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?=base_url('public/admin')?>/vendors/moment/min/moment.min.js"></script>
<script src="<?=base_url('public/admin')?>/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-wysiwyg -->
<script src="<?=base_url('public/admin')?>/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?=base_url('public/admin')?>/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="<?=base_url('public/admin')?>/vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="<?=base_url('public/admin')?>/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="<?=base_url('public/admin')?>/vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url('public/admin')?>/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="<?=base_url('public/admin')?>/vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="<?=base_url('public/admin')?>/vendors/autosize/dist/autosize.min.js"></script>
<!-- jQuery autocomplete -->
<script src="<?=base_url('public/admin')?>/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="<?=base_url('public/admin')?>/vendors/starrr/dist/starrr.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?=base_url('public/admin')?>/build/js/custom.min.js"></script>

<script src="<?=base_url('public/admin')?>/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="<?=base_url('public/admin')?>/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- easy-pie-chart -->
<script src="<?=base_url('public/admin')?>/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

<script src="<?=base_url('public/admin')?>/vendors/pnotify/dist/pnotify.js"></script>
<script src="<?=base_url('public/admin')?>/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?=base_url('public/admin')?>/vendors/swal.min.js"></script>
 <script src="<?=base_url('public/admin/')?>vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  <script src="<?=base_url('public/admin/')?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?=base_url('public/admin/')?>vendors/pdfmake/build/vfs_fonts.js"></script>

<script>
 $('#cointype').select2();
function notify(text){
  new PNotify({
    title: 'Success',
    text: text,
    type: 'success',
    hide: false,
    styling: 'bootstrap3'
  });
}
function NotifyDanger(text){
  new PNotify({
    title: 'Error',
    text: text,
    type: 'error',
    delay: 2500,
    hide: false,
    styling: 'bootstrap3'
  });
}
</script>
<?php
if(isset($flashdata)){
  ?>
  <script type="text/javascript">
  notify("<?=$flashdata?>");
  </script>
  <?php
}
?>
<?php
if(isset($flash_danger)){
  ?>
  <script type="text/javascript">
  NotifyDanger("<?=$flash_danger?>");
  </script>
  <?php
}
?>
<script>
function confirmDelete(url){
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      window.location.href=url;
    }
  })
}
$('#myDatepicker').datetimepicker();
$('#myDatepicker1').datetimepicker();
$('#myDatepicker2').datetimepicker({
  format: 'DD.MM.YYYY'
});

$('#myDatepicker3').datetimepicker({
  format: 'hh:mm A'
});

$('#myDatepicker4').datetimepicker({
  ignoreReadonly: true,
  allowInputToggle: true
});

$('#datetimepicker6').datetimepicker();

$('#datetimepicker7').datetimepicker({
  useCurrent: false
});

$("#datetimepicker6").on("dp.change", function(e) {
  $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
});

$("#datetimepicker7").on("dp.change", function(e) {
  $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
});
</script>

<script type="text/javascript">
    "use strict";

$(document).ready(function domReady() {
    $(".js-select2").select2({
        placeholder: "Pick states",
        theme: "material"
    });

    $(".select2-selection__arrow")
        .addClass("material-icons")
        .html("arrow_drop_down");
});
</script>
</body>
</html>
