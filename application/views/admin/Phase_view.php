<style>
.sort{
  list-style: none;
  border: 1px #ccc solid;
  color:#333;
  padding: 10px;
  margin-bottom: 4px;
}
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <?=$this->session->flashdata('error_msg');?>
        <?=$this->session->flashdata('item');?>

        </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
         <!--  <a href="" class="btn btn-danger btn-xs m-b-10 m-l-5"><i class="fa fa-plus"></i>&nbsp;category</a>

          <a href="" class="btn btn-success btn-xs m-b-10 m-l-5"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
        -->
          <div class="clearfix"></div>
          <a href="<?=base_url('Admin/phase')?>" Class="btn btn-info"><i class="fa fa-plus"></i>Add</a>
        </div>
        <div class="x_content">
          <table id="datatable" class="table table-striped">
            <thead style="background: rgba(52, 73, 94, 0.94);color: #ECF0F1;">
              <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Percentage Gain</th>
                <th>Action</th>

              </tr>
            </thead>
            <tbody>
              <?php $i=0;foreach ($return->result() as  $row) { $i++;?>
                <tr>
                  <td><?=$i?></td>
                  <td><?=$row->name?></td>
                  <td><?=$row->start_date?></td>
                  <td><?=$row->end_date?></td>
                  <td><?=$row->percentage_gain ?></td>

                  <!-- <td><a href="https://ropsten.etherscan.io/tx/<?=$row->txn_hash?>" target="_blank">View</td> -->
                  <td>
                  	<a href="<?=base_url('admin/phase_delete/'.$row->id)?>" class='btn btn-xs btn-danger' onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                  </td>

                  </tr>
                  <?php } ?>

                <tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

function delete_confirm(id){
    swal({
            title: "Are you sure to delete ?",
            text: "You will not be able to recover this !!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it !!",
            cancelButtonText: "No, cancel it !!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href="<?=base_url('category/delete/')?>"+id;
                 // swal("Deleted !!", id, "success");
            }
            else {
                swal("Cancelled !!", "", "error");
            }
        });
};
</script>
