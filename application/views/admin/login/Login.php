<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?=$title?></title>

  <!-- Bootstrap -->
  <link href="<?=base_url('public/admin/')?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?=base_url('public/admin/')?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="<?=base_url('public/admin/')?>vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- Animate.css -->
  <link href="<?=base_url('public/admin/')?>vendors/animate.css/animate.min.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="<?=base_url('public/admin/')?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="<?=base_url('public/admin/')?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="<?=base_url('public/admin/')?>build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
  <div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          <form method="post" action="<?=base_url('admin')?>">
            <h1>Login</h1>
            <!-- <input type="hidden" name="<?=$csrf_name;?>" value="<?=$csrf_hash;?>" /> -->
            <div>
              <input type="text" name="username" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
              <input type="password" name="password" class="form-control" placeholder="Password" required="" />
            </div>
            <!-- <div>
            <input type="password" name="operator" class="form-control" placeholder="operator" required="" />
          </div> -->
          <div>
            <input type="submit" name="submit" value="Log in" class="btn btn-default submit">
            <a class="reset_pass" href="#">Lost your password?</a>
          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <p class="change_link">New to site?
              <a href="#signup" class="to_register"> Create Account </a>
            </p>

            <div class="clearfix"></div>
            <br />

            <div>
              <h1><i class="fa fa-paw"></i>Bitcoin</h1>

            </div>
          </div>
        </form>
      </section>
    </div>

    <div id="register" class="animate form registration_form">
      <section class="login_content">
        <form>
          <h1>Create Account</h1>
          <div>
            <input type="text" class="form-control" placeholder="Username" required="" />
          </div>
          <div>
            <input type="text" name="text" class="form-control" placeholder="Username" required="" />
          </div>
          <div>
            <input type="password" name="operator" class="form-control" placeholder="Operator" required="" />
          </div>
          <div>
            <input type="password" name="password" class="form-control" placeholder="Password" required="" />
          </div>
          <div>
            <input type="submit" name="submit" class="btn btn-default submit">

          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <p class="change_link">Already a member ?
              <a href="#signin" class="to_register"> Log in </a>
            </p>

            <div class="clearfix"></div>
            <br />

            <div>
              <h1><i class="fa fa-paw"></i> Lottery!</h1>
              <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
</div>
</body>
<script src="<?=base_url('public/admin/')?>vendors/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url('public/admin/')?>vendors/pnotify/dist/pnotify.js"></script>
<script src="<?=base_url('public/admin/')?>vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?=base_url('public/admin/')?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
<?php
if(isset($flash))
{
  ?>
  <script>
  new PNotify({
    title: 'Oh No!',
    text: '<?=$flash?>',
    type: 'error',
    styling: 'bootstrap3'
  });
  </script>
  <?php
}
?>
</html>
