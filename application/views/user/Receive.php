<style>
.card [class*="card-header-"]:not(.card-header-icon):not(.card-header-text):not(.card-header-image) {
    border-radius: 1px;
    margin-top: -20px;
    padding: 15px;
}
.card .card-header .card-title {
    margin-bottom: 3px;
    text-transform: uppercase;
    font-size: 20px;
    text-align: center;
}
.card {
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    background: #fdfdfd;
    border-radius: 5px;
    min-height: 136px;
    /*border-right: 2px solid #fdfdfd;*/
}
.form-control {
    background: no-repeat center bottom, center calc(100% - 1px);
    background-size: 0 100%, 100% 100%;
    border: 1px solid #333;
    height: 36px;
    transition: background 0s ease-out;
    padding-left: 18px;
    padding-right: 0;
    border-radius: 25px;
    font-size: 14px;
    
}
.form-control:invalid {
    background-image: none;
}
.card .card-title {
    margin-top: -10;
    margin-bottom: 25px;
    text-align: center;
    color: #333;
    font-size: 21px;
    padding-top: 20px;
}
.card .card-body {
    padding: 0.9375rem 20px;
    position: relative;
    padding-bottom: 37px;
}
.form-control {
    background: no-repeat center bottom, center calc(100% - 1px);
    background-size: 0 100%, 100% 100%;
    border: 1px solid #333;
    height: 36px;
    transition: background 0s ease-out;
    padding-left: 18px;
    padding-right: 0;
    border-radius: 25px;
    font-size: 14px;
}
</style>

<div class="content">
  <div class="row">
    <div class="col-md-8" style="margin: auto;">
    <div class="container-fluid">
      <div class="card">
      <!--   <div class="card-header card-header-primary">
          <h3 class="card-title">Receive Token</h3>
        </div> -->
        <div class="card-body">
          <h3 class="card-title"><span style="color: #c51162">RECEIVE</span> TOKEN</h3>
          <div class="row">
            <img class="responsive-img" src="https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=<?=$address?>" style="margin: auto;min-height: 250px;min-width: 250px;border:1px solid #fdfdfd;">
            <div class="col-md-8" style="margin:auto;">
              <div class="form-group bmd-form-group">
                <input type="text" class="form-control white" value="<?=$address?>" id="myInput">
                <button type="submit" class="btn btn-primary" onclick="myFunction()" style="background: #C51162;border-radius: 25px;width: 100%; ">Copy<div class="ripple-container"></div></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div>
    </div>
  </div>
<script type="text/javascript">
function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
}
</script>
