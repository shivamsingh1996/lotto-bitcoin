<!DOCTYPE html>
<html>
<head>
<title>Login | Signup</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="<?=base_url('public/user/auth/')?>css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="<?=base_url('public/user/auth/')?>js/jquery-1.9.1.min.js"></script>
<!--// js -->
<link rel="stylesheet" type="text/css" href="<?=base_url('public/user/auth/')?>css/easy-responsive-tabs.css " />
 <link href="//fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
 <style>
 .bg {
    background: #171055;
    background-size: cover;
    background-attachment: fixed;
}
.w3_agile_login input[type="submit"]{
    width: 100%;
    text-transform: uppercase;
    outline: none;
    border: none;
    cursor: pointer;
    color: white !important;
    font-size: 0.85em;
    font-weight: 600;
    padding: 0.7em 0;
    letter-spacing: 1px;
    background: #c51162;
    border-radius: 25px;
}
.w3_agile_login input[type="submit"]:hover {
    width: 100%;
    text-transform: uppercase;
    outline: none;
    border: none;
    cursor: pointer;
    color: white !important;
    font-size: 0.85em;
    font-weight: 600;
    padding: 0.7em 0;
    letter-spacing: 1px;
    background: #c51162;
    border-radius: 25px;
}
.agile_its_registration input[type="submit"], .agile_its_registration input[type="reset"] {
    width: 49.4%;
    background: #c51162;
    outline: none;
    border: none;
    cursor: pointer;
    color: #000;
    font-weight: 600;
    text-transform: uppercase;
    padding: 0.6em 0;
    letter-spacing: 1px;
    font-size: 0.85em;
    line-height: 1.5;
}
.agile_its_registration input[type="submit"], .agile_its_registration input[type="reset"]:hover {
    width: 49.4%;
    background: #c51162;
    outline: none;
    border: none;
    cursor: pointer;
    color: #000;
    font-weight: 600;
    text-transform: uppercase;
    padding: 0.6em 0;
    letter-spacing: 1px;
    font-size: 0.85em;
    line-height: 1.5;
}
.w3_agile_login input[type="email"], .w3_agile_login input[type="password"], .agile_its_registration input[type="text"], .agile_its_registration input[type="password"], .agile_its_registration input[type="email"] {
    padding: 0.7em 1em;
    margin: 1em 0em 1em 0;
    width: 90.5%;
    /* background: transparent; */
    border: 1px solid #333;
    font-size: 0.85em;
    outline: none;
    letter-spacing: 1px;
    color: #c51162 !important;
    border-radius: 25px;
}
.wrap {
    padding: 2.5em;
    width: 25%;
    margin: 3em auto;
    background:#e8e8e8;
    border-radius: 5px;
}
.resp-tabs-list li {
    color: #333 !important;
    font-weight: 700;
    font-size: 1.1em !important;
    display: inline-block;
    padding: 0 13px 15px;
    list-style: none;
    cursor: pointer;
    letter-spacing: 2px;
}
.w3_agile_login p, .agile_its_registration p {
    font-size: 1em;
    color: #333;
}
.resp-tab-active {
    margin-bottom: -1px !important;
    padding: 0 14px 14px !important;
    border-bottom: 2px solid #c51162 !important;
}
.agile_its_registration input[type="submit"], .agile_its_registration input[type="reset"]:hover {
    width: 49.4%;
    background: #c51162;
    outline: none;
    border: none;
    cursor: pointer;
    color: #000;
    font-weight: 600;
    text-transform: uppercase;
    padding: 0.6em 0;
    letter-spacing: 1px;
    font-size: 0.85em;
    line-height: 1.5;
    border-radius: 25px;
}
.agile_its_registration input[type="submit"], .agile_its_registration input[type="reset"]{
    width: 49.4%;
    background: #c51162;
    outline: none;
    border: none;
    cursor: pointer;
    color: #000;
    font-weight: 600;
    text-transform: uppercase;
    padding: 0.6em 0;
    letter-spacing: 1px;
    font-size: 0.85em;
    line-height: 1.5;
    border-radius: 25px;
}

</style>
</head>
<body class="bg agileinfo">
   <div class="w3layouts_main wrap">
    <!--Horizontal Tab-->
        <div id="parentHorizontalTab_agile">
            <ul class="resp-tabs-list hor_1">
                <li>LOG IN</li>
                <li>SIGN UP</li>
            </ul>
            <div class="resp-tabs-container hor_1">
               <div class="w3_agile_login">
          <form action="<?=base_url('user/login')?>" method="post" class="agile_form">
            
            <input type="email" name="email" required="required" placeholder="Email" placeholder="Email" />
           
            <input type="password" name="password" required="required" class="password" placeholder="Password" /> 
            
            <input type="submit" name="submit" value="submit" class="agileinfo" style="margin-top: 20px;" />
          </form>
                    
                </div>
                <div class="agile_its_registration">
          <form action="<?=base_url('user/signup')?>" method="post" class="agile_form">
            
            <input type="text" name="fname" required="required" placeholder="First Name" />
            
            <input type="text" name="lname" required="required" placeholder="Last Name" />
            
            <input type="email" name="email" required="required" placeholder="Email" />
            
            <input type="text" name="username" required="required" placeholder="Username" />
            
            
            <input type="password" name="password" id="password1"  required="required" placeholder="Password">
            
            <input type="password" name="password_confirmation" id="password2"  required="required" placeholder="Confirm password">
            
            <input type="text" name="affiliate_from" placeholder="Affiliate Id" />
             <input type="submit" name="submit" value="submit" style="background: #c51162;color: #fdfdfd;">
             <input type="reset" value="Reset" style="background: #c51162;color: #fdfdfd;">
          </form> 
        </div>
            </div>
        </div>
     <!-- //Horizontal Tab -->
    </div>
  <!-- <div class="agileits_w3layouts_copyright text-center">
      <p>© 2017 Simple Login and Signup Form. All rights reserved | Design by <a href="//w3layouts.com/">W3layouts</a></p>
  </div> -->
<!--tabs-->
<script src="<?=base_url('public/user/auth/')?>js/easyResponsiveTabs.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  //Horizontal Tab
  $('#parentHorizontalTab_agile').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function(event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });
});
</script>
<script type="text/javascript">
    window.onload = function () {
      document.getElementById("password1").onchange = validatePassword;
      document.getElementById("password2").onchange = validatePassword;
    }
    function validatePassword(){
      var pass2=document.getElementById("password2").value;
      var pass1=document.getElementById("password1").value;
      if(pass1!=pass2)
        document.getElementById("password2").setCustomValidity("Passwords Don't Match");
      else
        document.getElementById("password2").setCustomValidity('');  
        //empty string means no validation error
    }

</script>
<!--//tabs-->
</body>
</html>