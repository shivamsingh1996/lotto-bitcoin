<style type="text/css">
  .coins{
    color:#333;
  }
  .card [class*="card-header-"]:not(.card-header-icon):not(.card-header-text):not(.card-header-image) {
    border-radius: 1px;
    margin-top: -20px;
    padding: 15px;
}
.card .card-header .card-title {
    margin-bottom: 3px;
    text-transform: uppercase;
    font-size: 20px;
    text-align: center;
}
.card {
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    background: #fdfdfd;
    border-radius: 5px;
    min-height: 136px;
    padding-bottom: 25px;
    /*border-right: 2px solid #fdfdfd;*/
}
.form-control {
    background: no-repeat center bottom, center calc(100% - 1px);
    background-size: 0 100%, 100% 100%;
    border: 1px solid #333;
    height: 36px;
    transition: background 0s ease-out;
    padding-left: 18px;
    padding-right: 0;
    border-radius: 25px;
    font-size: 14px;

}
.form-control:invalid {
    background-image: none;
}
/* width */
::-webkit-scrollbar {
    width: 8px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #fdfdfd;
}

/* Handle */
::-webkit-scrollbar-thumb {
    background: #333;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #040B3E;
}
.select{
      width: 100%;
    margin-top: 20px;
    height: 32px;
    border-radius: 25px;
    background: #fdfdfd;
    color: #333;
    border-color: #333;
}
.card .card-title {
    margin-top: 0;
    margin-bottom: 15px;
    text-align: center;
    color: #333;
    font-size: 21px;
    padding-top: 20px;
    text-transform: uppercase;
}
</style>
<?php if(!isset($purchase_value)) { ?>
<div class="content" style="margin-top: 20px;">
  <div class="row">
    <div class="col-md-8" style="margin:auto;">
    <div class="container-fluid">
      <div class="card">
        <!-- <div class="card-header card-header-primary">

        </div> -->
        <div class="card-body">
          <h3 class="card-title"><span style="color: #c51162">Purchase</span> Token</h3>
          <form action="<?=base_url('user/purchase_token')?>" method="post">
            <div class="row">
              <div class="col-md-8" style="margin:auto;">
                <div class="form-group bmd-form-group">
                  <input type="number" class="form-control" id="token_price"  placeholder="Enter Purchase Amount" name="token" required>
                </div>
              </div>
            </div>
            <div class="row col-md-8" id="coins" style="height: 200px;overflow-y: scroll;margin: auto;">
            </div>
<!--               <div class="col-md-6" >
                <div class="col-md-6">
                  <b>Bitcoin -</b>&nbsp;&nbsp;&nbsp;<span id="btc"></span>
                </div>
                <div class="col-md-6">
                  <b>Ether -</b>&nbsp;&nbsp;&nbsp; <span id="eth"></span>
                </div>
                <div class="col-md-6">
                  <b>Litcoin -</b>&nbsp;&nbsp;&nbsp;<span id="ltc"></span>
                </div>
                <div class="col-md-6">
                  <b>Ripple -</b>&nbsp;&nbsp;&nbsp;<span id="xrp"></span>
                </div>
                <div class="col-md-6">
                  <b>Zcash -</b>&nbsp;&nbsp;&nbsp;<span id="zec"> </span>
                </div>
              </div> -->

              <!-- <div class="col-md-6">
                <div class="col-md-6">
                  <b>Doge -</b>&nbsp;&nbsp;&nbsp;<span id="doge"></span>
                </div>
                <div class="col-md-6">
                  <b>Abyss -</b>&nbsp;&nbsp;&nbsp; <span id="abyss"></span>
                </div>
                <div class="col-md-6">
                  <b>AMP -</b>&nbsp;&nbsp;&nbsp;<span id="amp"></span>
                </div>
                <div class="col-md-6">
                  <b>Ripple -</b>&nbsp;&nbsp;&nbsp;<span id="xrp"></span>
                </div>
                <div class="col-md-6">
                  <b>Zcash -</b>&nbsp;&nbsp;&nbsp;<span id="zec"> </span>
                </div>
              </div> -->



           <!-- <div class="row">
                <div class="col-md-6">
                  <b>Bitcoin -</b>&nbsp;&nbsp;&nbsp;<span id="btc"></span>
                </div>
                <div class="col-md-6">
                  <b>Ether -</b>&nbsp;&nbsp;&nbsp; <span id="eth"></span>
                </div>
                <div class="col-md-6">
                  <b>Litcoin -</b>&nbsp;&nbsp;&nbsp;<span id="ltc"></span>
                </div>
                <div class="col-md-6">
                  <b>Ripple -</b>&nbsp;&nbsp;&nbsp;<span id="xrp"></span>
                </div>
                <div class="col-md-6">
                  <b>Zcash -</b>&nbsp;&nbsp;&nbsp;<span id="zec"> </span>
                </div>

            </div> -->




                 <div class="col-md-8 form-group" style="margin: auto;">
                  <select name="type" id="cointype" class="select" required>
                      <option value="">Choose mode of payment</option>
                      <?php
                      $data = json_decode($return_price, true);
                      $n = 0;
                      foreach ($data as $key => $value) {
                        $n++;
                        ?>
                        <option value="<?=$key?>"><?=$key?></option>
                        <?php
                      }
                    ?>
                  </select>
                </div>



            </div>
            <div class="row">
              <div class="col-md-8" style="margin: auto;text-align: center;">
                 <input type="submit" class="btn btn-primary" name="submit" value="Purchase" style="background: #C51162;border-radius: 25px;width:92%;line-height: 1;">
               </div>
             </div>

          </form>
        </div>
      </div>
    </div>
    <div>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function(){
  var coin_array = ['ETH','BTC','ZEC','XRP','ETC','LTC','DOGE','ABYSS','AMP','ANT','ARK','ARN','BAT','BNT','BRD','BTG','CFI','CVC','DCN','DCR','DGB','DNT','EDG','ELF','EXP','GNO','GUP','HMQ','KMD','LBC','LUN','MAID','MCO','MLN','NAV','NBT','NGC','NMR','NOAH','NXT','OMG','PAY','POT','PTOY','QTUM','RCN','REP','RLC','SALT','SNM','STORJ','STR','STX','SWT','SYS','TRST','USDT','VEN','VIB','WAX','XEM','XMO','ZCL','ZEN','ZRX'];
  var coin = '';
  for(var j = 0; j <coin_array.length ; j++){
    coin += '<div class="col-md-6" >';
      coin += '<div class="col-md-12 coins">=<span id="'+coin_array[j].toLowerCase()+'">0.00</span> <i class="cc '+coin_array[j]+'" title="'+coin_array[j]+'"></i><input type="hidden" name="coinname" value="'+coin_array[j]+'"><b>'+coin_array[j]+'</b></div>';
    coin += '</div>';
    }

  $("#coins").html(coin);
});
  $(document).on('click','.coins',function(){
    var id = $(this).find("input[name=coinname]").val();
    $("#cointype").val(id).trigger('change');
  })
//
  $(document).on("keyup","#token_price",function(){
    let token_price = $("#token_price").val();
    let token_api_price = JSON.parse('<?=$return_price?>');
    var count = Object.keys(token_api_price).length;
    console.log(count);
    // console.log(token_api_price);
     let ethVal = token_api_price.ETH;

    for (var key in token_api_price) {
      if (token_api_price.hasOwnProperty(key)) {
        var id = key.toLowerCase();
          $("#"+id).html((token_price * 1/token_api_price[key] * 1/ethVal).toFixed(4));
            console.log(token_price);
            console.log(1/token_api_price[key]);
            console.log(1/ethVal);
            // console.log(key + " -> " + token_api_price[key]);
        }
    }
    $("#eth").html((token_price * 1/ethVal).toFixed(4));

  })
  </script>
<?php }else { ?>

  <div class="content">
    <div class="row">
      <div class="col-md-6"
      <div class="container-fluid">
        <div class="card">
          <div class="card-header card-header-primary">
            <h3 class="card-title">Send <?=number_format((float)$purchase_value['value'], 6, '.', '')
.' '.$purchase_value['type']?> to this address</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <img class="responsive-img" src="https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=<?=$purchase_value['address']?>" style="margin-left:20px">
              <div class="col-md-12">
                <div class="form-group bmd-form-group">
                  <input type="text" class="form-control" value="<?=$purchase_value['address']?>" id="myInput">
                  <button type="submit" class="btn btn-primary pull-right" onclick="myFunction()">Copy<div class="ripple-container"></div></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
      </div>
    </div>
  <script type="text/javascript">
  function myFunction() {
    /* Get the text field */
    var copyText = document.getElementById("myInput");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");
  }
  </script>
<?php } ?>
