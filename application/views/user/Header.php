
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="..<?=base_url('public/user')?>/img/apple-icon.png">
  <link rel="icon" type="image/png" href="..<?=base_url('public/user')?>/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    <?=$title?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->
  <link rel="canonical" href="https://www.creative-tim.com/product/material-dashboard" />
  <!--  Social tags      -->

  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url('public/user')?>/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="..<?=base_url('public/user')?>/demo/demo.css" rel="stylesheet" />
  <!-- Google Tag Manager -->
    <script src="<?=base_url('public/user')?>/js/core/jquery.min.js" type="text/javascript"></script>
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NKDMSK6');
  </script>
  <!-- End Google Tag Manager -->
  <style>
  /*preloader*/
/**
 * Google Material Design Preloader
 *
 * CSS animated SVG implementation of the Google Material Design preloader
 *
 * License: MIT
 * Author: Rudi Theunissen (rudolf.theunissen@gmail.com)
 * Version: 1.1.0
 */
.md-preloader {
  font-size: 0;
  display: inline-block;
  -webkit-animation: outer 6600ms linear infinite;
          animation: outer 6600ms linear infinite;
}
.md-preloader svg {
  -webkit-animation: inner 1320ms linear infinite;
          animation: inner 1320ms linear infinite;
}
.md-preloader svg circle {
  fill: none;
  stroke: #448AFF;
  stroke-linecap: square;
  -webkit-animation: arc 1320ms cubic-bezier(0.8, 0, 0.4, 0.8) infinite;
          animation: arc 1320ms cubic-bezier(0.8, 0, 0.4, 0.8) infinite;
}
@-webkit-keyframes outer {
  0% {
    -webkit-transform: rotate(0);
            transform: rotate(0);
  }
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@keyframes outer {
  0% {
    -webkit-transform: rotate(0);
            transform: rotate(0);
  }
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@-webkit-keyframes inner {
  0% {
    -webkit-transform: rotate(-100.8deg);
            transform: rotate(-100.8deg);
  }
  100% {
    -webkit-transform: rotate(0);
            transform: rotate(0);
  }
}
@keyframes inner {
  0% {
    -webkit-transform: rotate(-100.8deg);
            transform: rotate(-100.8deg);
  }
  100% {
    -webkit-transform: rotate(0);
            transform: rotate(0);
  }
}
@-webkit-keyframes arc {
  0% {
    stroke-dasharray: 1 210.48670779px;
    stroke-dashoffset: 0;
  }
  40% {
    stroke-dasharray: 151.55042961px, 210.48670779px;
    stroke-dashoffset: 0;
  }
  100% {
    stroke-dasharray: 1 210.48670779px;
    stroke-dashoffset: -151.55042961px;
  }
}
@keyframes arc {
  0% {
    stroke-dasharray: 1 210.48670779px;
    stroke-dashoffset: 0;
  }
  40% {
    stroke-dasharray: 151.55042961px, 210.48670779px;
    stroke-dashoffset: 0;
  }
  100% {
    stroke-dasharray: 1 210.48670779px;
    stroke-dashoffset: -151.55042961px;
  }
}

  /*end*/
   .sidebar .sidebar-wrapper {
    position: relative;
    height: calc(100vh - 75px);
    overflow: auto;
    width: 260px;
    z-index: 4;
    padding-bottom: 30px;
    background: #171055;
    height: 100%;
}
.sidebar .nav p {
    margin: 0;
    line-height: 30px;
    font-size: 14px;
    position: relative;
    display: block;
    height: auto;
    white-space: nowrap;
    color: #fdfdfd;
    font-weight: normal;
    text-transform: uppercase;
}
.sidebar[data-color="purple"] li.active>a {
    background-color: #C51162;
    box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(156, 39, 176, 0.4);
    border-radius: 0px;
    /* color: #fff !important; */
}
.main-panel{
  /*background: #201e33;*/
}
.white{
  color: #333;

}
.form-control {
    background: no-repeat center bottom, center calc(100% - 1px);
    background-size: 0 100%, 100% 100%;
    border: 1px solid #333;
    height: 36px;
    transition: background 0s ease-out;
    padding-left: 18px;
    padding-right: 0;
    border-radius: 25px;
    font-size: 14px;
}
/*body{
  background: #040A2C;
}*/
/*.card {
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    background: #302742;
}*/
.card-stats .card-header .card-category:not([class*="text-"]) {
    color: #333;
    font-size: 16px;
    font-weight: 500;
}
.active{
  background: none;
}
.sidebar .logo {
    padding: 15px 0px;
    margin: 0;
    display: block;
    position: relative;
    z-index: 4;
    background: #171055;
}


#loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   z-index: 90;
   background-color: #fff;
   opacity: 0.7;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 30%;
  left: 50%;
   z-index: 100;
}
.hide{
  display: none;
}
 </style>
 <link rel="stylesheet" href="<?=base_url()?>public/user/webfont/cryptocoins.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

</head>

<body class="">

  <!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="..<?=base_url('public/user')?>/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?=base_url('user/token')?>" class="simple-text logo-normal" style="color: #fdfdfd;">
          Token
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?=$this->uri->segment(2) == 'dashboard' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url('user/dashboard')?>">
              <i class="material-icons white">dashboard</i>
              <p class="white">Dashboard</p>
            </a>
          </li>
          <li class="nav-item <?=$this->uri->segment(2) == 'purchase_token' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url('user/purchase_token')?>" id="purchase_token">
              <i class="material-icons white">add_shopping_cart</i>
              <p class="white">Purchase Token</p>
            </a>
          </li>
          <li class="nav-item <?=$this->uri->segment(2) == 'send_token' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url('user/send_token')?>">
              <i class="material-icons white">send</i>
              <p class="white">Send Token</p>
            </a>
          </li>
          <li class="nav-item <?=$this->uri->segment(2) == 'receive' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url('user/receive')?>">
              <i class="material-icons white">library_books</i>
              <p class="white">Receive Token</p>
            </a>
          </li>
          <?php
            $user_id = $_SESSION['user_id'];
            $db = &get_instance();
            $return = $db->db->query("select * from address where user_id = $user_id");
            $address = $return->result()[0]->eth_add;
           ?>
          <li class="nav-item ">
            <a class="nav-link" href="https://ropsten.etherscan.io/address/<?=$address?>" target="_blank">
              <i class="material-icons white">bubble_chart</i>
              <p class="white">History</p>
            </a>
          </li>
          <li class="nav-item <?=$this->uri->segment(2) == 'change_password' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url('user/change_password')?>">
              <i class="material-icons white">security</i>
              <p class="white">Change Password</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?=base_url('user/logout')?>">
              <i class="material-icons white">power_settings_new</i>
              <p class="white">Logout</p>
            </a>
          </li>
          <!-- <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
        <div class="container-fluid">
         <!--  <div class="navbar-wrapper">
          <a class="navbar-brand" href=""> Dashboard</a>
        </div> -->

          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">


          </div>
        </div>
      </nav>
    <div id="loading" style="display:none;">
      <div id="loading-image">
        <img src="<?=base_url()?>public/admin/images/spinner.gif" id="load_img">
        <h4 style="font-weight:600;color:#C51162">Fetching Rates....</h4>
      </div>
    </div>
